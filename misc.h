// misc.h

#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#define CLIENT_TIMEOUT_SEC 15
#define MAX_SOCKETS 255
#define MAX_CLIENTS MAX_SOCKETS - 1
#define BUF_SIZE 1024

#define DEBUG



#ifdef DEBUG
#define DEBUG_LOG1(s)({ printf(s); })
#define DEBUG_LOG2(s, arg1)({ printf(s, arg1); })
#define DEBUG_LOG3(s, arg1, arg2)({ printf(s, arg1, arg2); })
#endif

#ifndef DEBUG
#define DEBUG_LOG1(s)({ /* nothing */ })
#define DEBUG_LOG2(s, arg1)({ /* nothing */ })
#define DEBUG_LOG3(s, arg1, arg2)({ /* nothing */ })
#endif

#define FATAL()({ err(1, "%s\n", strerror(errno)); })
#define FATAL1(str)({ err(1, "%s : %s\n", str, strerror(errno)); })


