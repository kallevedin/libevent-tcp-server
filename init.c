// init.c

#include <stdlib.h>
#include <stdio.h>

#include "misc.h"
#include "server.h"

#ifdef __OpenBSD__
    // for pledge, unveil
#include <unistd.h>
#endif

#ifdef __linux__
    // for strtonum
#include <limits.h>
#include <bsd/stdlib.h>
#endif


int main(int argc, char** argv) {
    // TODO: Parse arguments and derp herp

#ifdef __OpenBSD__
    if (unveil("/", ""))
        FATAL();
    if (pledge("stdio inet", ""))
        FATAL();
#endif

    const char* err_str;
    int listen_port = 9997;

    if (argc == 2) {
        listen_port = strtonum(argv[1], 1, 65335, &err_str);
        if (err_str != NULL) {
            printf("Port must be in the interval 1-65335. \"%s\" is: %s\n", argv[1], err_str);
            exit(1);
        }
    }

    start_server(listen_port);
}
