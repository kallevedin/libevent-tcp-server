// client.c

#include <event.h>
#include <netinet/tcp.h>
#include <stdlib.h>
#include <sys/socket.h>

#ifdef __linux__
#include <bsd/sys/tree.h>
#endif
#ifdef __OpenBSD__
#include <sys/tree.h>
#endif

#include <unistd.h>

#include "client.h"
#include "misc.h"
#include "server.h"

char* banner = "\x1b[7m\x1b[31mHERP DERP\x1b[0m\n\x1b[7m\x1b[31mDERP HERP\x1b[0m\n\n";

long long next_client_id = 0;

struct client_node {
    RB_ENTRY(client_node) entry;
    int fd;
    struct client *c;
};

int client_new(int fd, struct sockaddr_in *addr);
int client_destroy(struct client *client, int close_socket);
int client_node_cmp(struct client_node *e1, struct client_node *e2);
void client_handle_event(int fd, short what, void *client);
void client_once_per_sec();
struct client_node *get_client_node(int fd); 
struct client *get_client(int fd);
void client_write_buffer(struct client *c, char *data, ssize_t len, enum segment_flags flags);
void client_write(struct client *c, char *data, ssize_t len);
void client_write_static(struct client *c, char *data, ssize_t len);
void update_event(struct client *client, short what);
void read_from_client(struct client *c);
void write_client_buffer(struct client *c);

// NAME = client_tree
// TYPE = client_node
// FIELD = entry
// CMP = client_node_cmp
// see "man 3 tree"
RB_HEAD(client_tree, client_node) client_tree_head = RB_INITIALIZER(&client_tree_head);
RB_PROTOTYPE(client_tree, client_node, entry, client_node_cmp)
RB_GENERATE(client_tree, client_node, entry, client_node_cmp)



#ifdef DEBUG
void wrapped_print_tree(struct client_node *n) {
    struct client_node *left, *right;
    if (n == NULL) {
        printf("nil");
        return;
    }
    left = RB_LEFT(n, entry);
    right = RB_RIGHT(n, entry);
    if (left == NULL && right == NULL) {
        printf("[client (fd:%d) %lld]", n->c->fd, n->c->id);
    } else {
        printf("[client (fd:%d) %lld] ( ", n->c->fd, n->c->id);
        wrapped_print_tree(left);
        printf(" , ");
        wrapped_print_tree(right);
        printf(" ) ");
    }
}

void print_tree() {
    wrapped_print_tree(RB_ROOT(&client_tree_head));
    printf("\n");
}
#endif



int client_new(int fd, struct sockaddr_in *addr) {
    int on = 1;
    struct client_node *cn;
    struct client *c;

    DEBUG_LOG2("creating client for fd=%d\n", fd);

    if (NULL != get_client_node(fd)) {
        DEBUG_LOG2("file descriptor %d re-used\n", fd);
        FATAL();
    }

    if (!(cn = malloc(sizeof(struct client_node))))
        FATAL();
    if (!(c = malloc(sizeof(struct client))))
        FATAL();

    cn->c = c;
    cn->fd = fd;

    c->id = next_client_id++;
    c->fd = fd;
    c->addr = addr;
    c->state = NEW_CONNECTION;
    c->event = malloc(sizeof(struct event));
    c->event_what = EV_READ|EV_WRITE|EV_PERSIST;
    c->write_buf_len = 0;
    if (gettimeofday(&c->last_read_timestamp, NULL))
        FATAL();
    SIMPLEQ_INIT(&c->write_buf_head);

    setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (void *)&on, sizeof(int));

    RB_INSERT(client_tree, &client_tree_head, cn);

    event_set(c->event, c->fd, c->event_what, &client_handle_event, c);
    event_add(c->event, NULL);

    client_write_static(c, banner, strlen(banner));
    return 0;
}

int client_destroy(struct client *c, int close_socket) {
    struct buf_entry *be;

    DEBUG_LOG2("terminating client: %d\n", c->fd);
    event_del(c->event);
    while (!SIMPLEQ_EMPTY(&c->write_buf_head)) {
        be = SIMPLEQ_FIRST(&c->write_buf_head);
        SIMPLEQ_REMOVE_HEAD(&c->write_buf_head, entry);
        if (be->flags & SHOULD_FREE)
            free(be->segment);
        free(be);
    }
    RB_REMOVE(client_tree, &client_tree_head, get_client_node(c->fd));
    if (close_socket)
        if (close(c->fd))
            FATAL();

    free(c);
    return 0;
}

int client_node_cmp(struct client_node *cn1, struct client_node *cn2) {
    int a = cn1->fd;
    int b = cn2->fd;

    if (a > b) return 1;
    else if (a < b) return -1;
    return 0;
}

void client_handle_event(int fd, short what, void *arg) {
    struct client *c = arg;

    DEBUG_LOG3("handling client on fd=%d, id=%lld\n", fd, c->id);

    if (what & EV_READ)
        read_from_client(c);
    if (what & EV_WRITE)
        write_client_buffer(c);
}

// is called about once per second
void client_once_per_sec() {
    struct client_node *cn;
    struct timeval now;
    gettimeofday(&now, NULL);

    time_t now_sec = now.tv_sec;
    RB_FOREACH(cn, client_tree, &client_tree_head) {
        struct client *c = cn->c;
        time_t c_sec = c->last_read_timestamp.tv_sec;
        time_t sec_since_last_read = now_sec - c_sec;

        if (sec_since_last_read > CLIENT_TIMEOUT_SEC) {
            DEBUG_LOG3("client timeout (id=%lld, fd=%d)\n", c->id, c->fd);
            client_destroy(c, 0);
        }
    }
}

struct client_node *get_client_node(int fd) {
    struct client_node find;
    find.fd = fd;

    return RB_FIND(client_tree, &client_tree_head, &find);
}

// returns the client with this fd, or NULL
struct client *get_client(int fd) {
    struct client_node *cn = get_client_node(fd);
    return cn ? cn->c : NULL;
}

void client_write_buffer(struct client *c, char *data, ssize_t len, enum segment_flags flags) {
    struct buf_entry *b;
    if (!(b = malloc(sizeof(struct buf_entry))))
        FATAL();

    b->segment_len = len;
    b->written = 0;
    b->segment = data;
    b->flags = flags;
    c->write_buf_len += len; // total len

    SIMPLEQ_INSERT_TAIL(&c->write_buf_head, b, entry);
}

// data will be free()d after it has been written
void client_write(struct client *c, char *data, ssize_t len) {
    client_write_buffer(c, data, len, SHOULD_FREE);
    update_event(c, c->event_what|EV_WRITE);
}

// data will NOT be free()d after it has been written
void client_write_static(struct client *c, char *data, ssize_t len) {
    client_write_buffer(c, data, len, DONT_FREE);
    update_event(c, c->event_what|EV_WRITE);
}

void update_event(struct client *c, short what) {
    if (c->event_what == what)
        return;

    DEBUG_LOG3("updating event for client (fd:%d) %lld\n", c->fd, c->id);

    event_del(c->event);
    event_set(c->event, c->fd, what, &client_handle_event, c);
    event_add(c->event, NULL);

    c->event_what = what;
}

void read_from_client(struct client *c) {
    struct client_node *cn;
    ssize_t size;
    char *buf;

    DEBUG_LOG3("read event for client (fd:%d) %lld\n", c->fd, c->id);
    if (!(buf = malloc(BUF_SIZE)))
        FATAL();

    size = read(c->fd, buf, BUF_SIZE);

    if (!size) {
        DEBUG_LOG3("client disconnected (fd:%d) %lld\n", c->fd, c->id);
        client_destroy(c, 1);
        free(buf);
        return;
    }

    if (gettimeofday(&c->last_read_timestamp, NULL))
        FATAL();

    RB_FOREACH(cn, client_tree, &client_tree_head) {
        if (cn->c->fd != c->fd) {
            char *c_buf = malloc(size);
            memcpy(c_buf, buf, size);
            client_write(cn->c, c_buf, size);
        }
    }
    free(buf);
}

// writes data from the client write buffer
void write_client_buffer(struct client *c) {
    ssize_t size;

    if (c->write_buf_len != 0) {
        // empty out the client buffer
        // write one message (buffer segment) at a time...
        // if not all data could be written, then advance the offset pointer (b->written)
        struct buf_entry *b = SIMPLEQ_FIRST(&c->write_buf_head);
        void *buf_ptr = (void *)((char *)b->segment + b->written);
        ssize_t remaining_len = b->segment_len - b->written;

        if ((size = write(c->fd, buf_ptr, remaining_len)) < 0)
            FATAL();

        b->written += size;
        c->write_buf_len -= size;

        if (b->written > b->segment_len || c->write_buf_len < 0) {
            DEBUG_LOG3("emptying client buffer entry failed!!! wrote past end!? size=%zd, b->written=%zd\n", size, b->written);
            DEBUG_LOG2("    c->write_buf_len=%zd\n", c->write_buf_len);
            exit(1);
        } else if (b->written < b->segment_len) {
            return;
        }

        SIMPLEQ_REMOVE_HEAD(&c->write_buf_head, entry);
        if (b->flags & SHOULD_FREE)
            free(b->segment);
        free(b);
    }
    if (!c->write_buf_len)
        update_event(c, c->event_what & (~EV_WRITE));
}


