// client.h

#include <event.h>
#include <netinet/in.h>
#include <sys/queue.h>

enum client_state {
    NEW_CONNECTION,
    DEFAULT,
};

enum segment_flags {
    DONT_FREE = 0,
    SHOULD_FREE = 1,
};

struct buf_entry {
    ssize_t segment_len;
    ssize_t written;
    enum segment_flags flags;
    char *segment;
    SIMPLEQ_ENTRY(buf_entry) entry;
};

struct client {
    long long id;
    int fd;
    struct sockaddr_in *addr;
    enum client_state state;
    struct event *event;
    short event_what;

    // seconds and microseconds since Epoch
    struct timeval last_read_timestamp;

    ssize_t write_buf_len;
    SIMPLEQ_HEAD(buf_head, buf_entry) write_buf_head; 
};

int client_new(int fd, struct sockaddr_in*);
int client_terminate(struct client *client, int close_socket);
void client_once_per_sec();
