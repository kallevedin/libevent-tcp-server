#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/ip.h>

#include <event.h>

#include "misc.h"
#include "server.h"
#include "client.h"



int server_sock_fd;
int server_port;

static struct sockaddr_in server_sock_addr;

struct event_base *event_base;
struct event serv_sock_event;

static struct timeval one_second = { 1, 0 };



void start_server(int listen_port);
 
static int create_server_sock(int port); 
static void handle_signal(int signum, short what, void *arg);
static void every_second(int unknown, short what, void *arg); 
static void server_socket_event(int sock, short what, void *arg);
static int create_server_sock(int port);
 


void start_server(int listen_port) {
    struct event *sigint_event;
    struct event *sighup_event;
    struct event *every_second_event;
    server_port = listen_port;

#ifdef DEBUG
    setbuf(stdout, NULL);
#endif

    if (!(event_base = event_init()))
        FATAL1("failed to initalize libevent");

    if (!(sigint_event = malloc(sizeof(struct event))))
        FATAL();
    if (!(sighup_event = malloc(sizeof(struct event))))
        FATAL();
     if (!(every_second_event = malloc(sizeof(struct event))))
        FATAL();

    signal_set(sigint_event, SIGINT, &handle_signal, sigint_event);
    signal_add(sigint_event, NULL);
    signal_set(sighup_event, SIGHUP, &handle_signal, sighup_event);
    signal_add(sighup_event, NULL);

    evtimer_set(every_second_event, &every_second, every_second_event);
    evtimer_add(every_second_event, &one_second);

	server_sock_fd = create_server_sock(server_port);
    DEBUG_LOG2("server socket listening at port %d\n", server_port);

    event_set(&serv_sock_event, server_sock_fd, EV_PERSIST|EV_READ, &server_socket_event, NULL);
    event_add(&serv_sock_event, NULL);
    DEBUG_LOG2("watching server sock fd=%d for read-events\n", server_sock_fd);

    event_dispatch();
    DEBUG_LOG1("libevent exited");
}

// libevent makes this safe, we dont have to care about the obscureness that applies
// to normal raw signal handlers.
void handle_signal(int signum, short what, void *arg) {
    // struct event *e = (struct event *)arg;
    if (signum == SIGINT) {
        // terminate program
        DEBUG_LOG1("SIGINT\n");
        exit(0);
    } else if (signum == SIGHUP) {
        // reload configuration files and whatnot
        DEBUG_LOG1("SIGHUP\n");
    } else {
        DEBUG_LOG2("Unknown signal: %d\n", signum);
        exit(1);
    }
}

// don't trust that this is EXACTLY one second interval
static void every_second(int unknown, short what, void *arg) {
    struct event *e = (struct event *) arg;
    evtimer_add(e, &one_second);
    client_once_per_sec();
    // DEBUG_LOG1("tick\n");
}

// handle clients connecting
static void server_socket_event(int sock_fd, short what, void *arg) {
    int client_fd;
    struct sockaddr_in *addr;
    socklen_t addr_len = sizeof(struct sockaddr_in);
    
    addr = malloc(addr_len);

    DEBUG_LOG1("server socket event\n");
    if ((client_fd = accept(sock_fd, (struct sockaddr *) addr, &addr_len)) == -1)
        FATAL1("failed to accept new client");

    client_new(client_fd, addr);
}

// create non-blocking listening sock
// this is used for accepting incomming connections
static int create_server_sock(int port) {
    int sock_fd;
    int enable = 1;

    memset(&server_sock_addr, 0, sizeof(struct sockaddr_in));
    server_sock_addr.sin_family = AF_INET;
    server_sock_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_sock_addr.sin_port = htons(port);

    if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        FATAL1("failed to open sock");

    if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, (int *) &enable, sizeof(int)) < 0)
        FATAL1("failed to set SO_REUSEADDR");

    if (ioctl(sock_fd, FIONBIO, (int *) &enable) < 0)
        FATAL1("failed to make server socket non-blocking");    

    if (bind(sock_fd, (struct sockaddr *) &server_sock_addr, sizeof(server_sock_addr)))
        FATAL1("failed to bind server socket");

    if (listen(sock_fd, 10) == -1)
        FATAL1("failed to listen");

    return sock_fd;
}

