# C TCP server #

A TCP server that uses [libevent](https://libevent.org/) to listen on and react to events on multiple non-blocking sockets simultanously. It runs in a single thread/process.

It is essentially a sloppy translation of [DerpyTelnetServer](https://bitbucket.org/kallevedin/derpytelnetserver/) to C.

### Operating systems ###
* OpenBSD
* Linux

It requires **libbsd** to compile under Linux.

### License ###
[Creative Commons Zero](https://creativecommons.org/share-your-work/public-domain/cc0). This essentially means that you can do whatever you want with the code, including copying it and claiming that you wrote it. However, if you do that, various timestamps will show that I wrote it first :P

### Authors ###
* Kalle Vedin

